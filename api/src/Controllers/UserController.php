<?php

namespace App\Controllers;

use App\Config\ResponseHttp;
use App\Config\Security;
use App\Models\UserModel;
use Rakit\Validation\Validator;

class UserController extends BaseController
{

    /**************************Login**************************/
    final public function postLogin(string $endPoint)
    {

        if ($this->getMethod() == 'post' && $endPoint == $this->getRoute()) {

            $validator = new Validator([
                'login_usuario' => 'login_usuario es requerido',
                'login_clave' => 'login_clave es requerido',
            ]);

            $validation = $validator->validate($this->getParam(), [
                'login_usuario' => 'required',
                'login_clave' => 'required',
            ]);


            if ($validation->fails()) {
                $errors = $validation->errors();
                echo json_encode(ResponseHttp::status400($message=$errors->all()[0]));
            } else {
                $datos = $this->getParam();

                UserModel::setUsuario($datos['login_usuario']);
                UserModel::setPassword($datos['login_clave']);
                echo json_encode(UserModel::login());
            }

            exit;
        }
    }


    /**************************Consultar todos los usuarios**************************/
    final public function getAll(string $endPoint)
    {
        if ($this->getMethod() == 'get' && $endPoint == $this->getRoute()) {
            Security::validateTokenJwt(Security::secretKey());
            echo json_encode(UserModel::getAll());
            exit;
        }
    }

    /**************************Consultar un usuario por ID**************************/
    final public function getUser(string $endPoint)
    {

        if ($this->getMethod() == 'get' && $endPoint == $this->getRoute()) {
            Security::validateTokenJwt(Security::secretKey());
            $id = $this->getAttribute()[1];
            if (!isset($id)) {
                echo json_encode(ResponseHttp::status400('El ID es requerido'));
            } else if (!preg_match(self::$validate_number, $id)) {
                echo json_encode(ResponseHttp::status400('El ID solo admite números'));
            } else {
                UserModel::setId($id);
                echo json_encode(UserModel::getUser());
                exit;
            }
            exit;
        }
    }

    /**************************Consultar  usuarios por Texto**************************/
    final public function getUserByString(string $endPoint)
    {
        $texto = $_GET['query']??'';
        if ($this->getMethod() == 'get' && $endPoint == $this->getRoute()) {
            Security::validateTokenJwt(Security::secretKey());
            if (!isset($texto) || empty($texto)) {
                echo json_encode(ResponseHttp::status400('query es requerido'));
            } else {
                UserModel::setQueryValue($_GET['query']);
                echo json_encode(UserModel::getUserByString());
                exit;
            }
            exit;
        }
    }

    /**************************Registrar usuario**************************/
    final public function postSave(string $endPoint)
    {
        if ($this->getMethod() == 'post' && $endPoint == $this->getRoute()) {

            $validator = new Validator;

            $validator->setMessage('required', ":attribute es requerido.");
            $validator->setMessage('same', "Las CLAVES que ha ingresado no coinciden.");
            $validator->setMessage('email', "Ha ingresado un correo electrónico no válido.");
            $validator->setMessage('min', ":attribute , debe ser mínimo :min caracteres.");
            $validator->setMessage('max', ":attribute , debe ser máximo :max caracteres.");

            $validation = $validator->validate($this->getParam(), [
                'usuario_nombre'        => 'required|min:3|max:40|regex:/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/',
                'usuario_apellido'      => 'required|min:3|max:40|regex:/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/',
                'usuario_usuario'       => 'required|min:3|max:40|regex:/^[a-zA-Z0-9]+$/',
                'usuario_email'         => 'required|email|min:3|max:70',
                'usuario_clave_1'       => 'required|min:7|max:100|regex:/^[a-zA-Z0-9$@.-]+$/',
                'usuario_clave_2'       => 'required|min:7|max:100|regex:/^[a-zA-Z0-9$@.-]+$/|same:usuario_clave_1'
            ]);

            if ($validation->fails()) {
                $errors = $validation->errors();
                echo json_encode(ResponseHttp::status400($errors->all()[0]));
            } else {
                UserModel::setNombre($this->getParam()['usuario_nombre']);
                UserModel::setApellido($this->getParam()['usuario_apellido']);
                UserModel::setUsuario($this->getParam()['usuario_usuario']);
                UserModel::setEmail($this->getParam()['usuario_email']);
                UserModel::setPassword($this->getParam()['usuario_clave_1']);
                echo json_encode(UserModel::postSave());
            }

            exit;
        }
    }

    /**************************Actualizar Usuario**************************/
    final public function patchUser(string $endPoint)
    {
        if ($this->getMethod() == 'patch' && $this->getRoute() == $endPoint) {

            $validator = new Validator;

            $validator->setMessage('required', ":attribute es requerido.");
            $validator->setMessage('required_with', ":attribute es requerido.");
            $validator->setMessage('integer', ":attribute debe ser número entero.");
            $validator->setMessage('same', "Las CLAVES que ha ingresado no coinciden.");
            $validator->setMessage('email', "Ha ingresado un correo electrónico no válido.");
            $validator->setMessage('min', ":attribute , debe ser mínimo :min caracteres.");
            $validator->setMessage('max', ":attribute , debe ser máximo :max caracteres.");

            $validation = $validator->validate($this->getParam(), [
                'id'        => 'required|integer',
                'usuario_nombre'        => 'required|min:3|max:40|regex:/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/',
                'usuario_apellido'      => 'required|min:3|max:40|regex:/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/',
                'usuario_usuario'       => 'required|min:3|max:40|regex:/^[a-zA-Z0-9]+$/',
                'usuario_email'         => 'required|email|min:3|max:70',
                'usuario_clave_1'       => 'min:7|max:100|regex:/^[a-zA-Z0-9$@.-]+$/',
                'usuario_clave_2'       => 'required_with:usuario_clave_1|min:7|max:100|regex:/^[a-zA-Z0-9$@.-]+$/|same:usuario_clave_1'
            ]);

            if ($validation->fails()) {
                $errors = $validation->errors();
                echo json_encode(ResponseHttp::status400($errors->all()[0]));
            } else {
                UserModel::setId($this->getParam()['id']);
                UserModel::setNombre($this->getParam()['usuario_nombre']);
                UserModel::setApellido($this->getParam()['usuario_apellido']);
                UserModel::setUsuario($this->getParam()['usuario_usuario']);
                UserModel::setEmail($this->getParam()['usuario_email']);
                UserModel::setPassword($this->getParam()['usuario_clave_1']);
                echo json_encode(UserModel::patchUser());
            }

            exit;
        }
    }
    /**************************Actualizar contraseña de usuario**************************/
    final public function patchPassword(string $endPoint)
    {
        if ($this->getMethod() == 'patch' && $this->getRoute() == $endPoint) {
            Security::validateTokenJwt(Security::secretKey());

            $jwtUserData = Security::getDataJwt();

            if (empty($this->getParam()['oldPassword']) || empty($this->getParam()['newPassword']) || empty($this->getParam()['confirmNewPassword'])) {
                echo json_encode(ResponseHttp::status400('Todos los campos son requeridos'));
            } else if (!UserModel::validateUserPassword($jwtUserData['IDToken'], $this->getParam()['oldPassword'])) {
                echo json_encode(ResponseHttp::status400('La contraseña antigua no coincide'));
            } else if (strlen($this->getParam()['newPassword']) < 8 || strlen($this->getParam()['confirmNewPassword']) < 8) {
                echo json_encode(ResponseHttp::status400('La contraseña debe tener un minimo de 8 caracteres'));
            } else if ($this->getParam()['newPassword'] !== $this->getParam()['confirmNewPassword']) {
                echo json_encode(ResponseHttp::status400('Las contraseñas no coinciden'));
            } else {
                UserModel::setIDToken($jwtUserData['IDToken']);
                UserModel::setPassword($this->getParam()['newPassword']);
                echo json_encode(UserModel::patchPassword());
            }
            exit;
        }
    }

    /**************************Eliminar usuario**************************/
    final public function deleteUser(string $endPoint)
    {
        if ($this->getMethod() == 'delete' && $this->getRoute() == $endPoint) {
            Security::validateTokenJwt(Security::secretKey());

            if (empty($this->getParam()['usuario_id'])) {
                echo json_encode(ResponseHttp::status400('Todos los campos son requeridos'));
            } else {
                UserModel::setId($this->getParam()['usuario_id']);
                echo json_encode(UserModel::deleteUser());
            }
            exit;
        }
    }
}
