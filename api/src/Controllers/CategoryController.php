<?php

namespace App\Controllers;

use App\Config\ResponseHttp;
use App\Config\Security;
use App\Models\CategoryModel;
use Rakit\Validation\Validator;

class CategoryController extends BaseController
{

    /**************************Listado**************************/
    final public function getAll(string $endPoint)
    {
        if ($this->getMethod() == 'get' && $endPoint == $this->getRoute()) {
            Security::validateTokenJwt(Security::secretKey());
            echo json_encode(CategoryModel::getAll());
            exit;
        }
    }


    /************************** Nuevo **************************/
    final public function postSave(string $endPoint)
    {
        if ($this->getMethod() == 'post' && $endPoint == $this->getRoute()) {
            Security::validateTokenJwt(Security::secretKey());
            $validator = new Validator;

            $validator->setMessage('required', ":attribute es requerido.");
            $validator->setMessage('min', ":attribute , debe ser mínimo :min caracteres.");
            $validator->setMessage('max', ":attribute , debe ser máximo :max caracteres.");

            $validation = $validator->validate($this->getParam(), [
                'nombre'        => 'required|min:4|max:50|regex:/^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ ]+$/',
                'ubicacion'      => 'required|min:4|max:50|regex:/^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ ]+$/',
            ]);

            if ($validation->fails()) {
                $errors = $validation->errors();
                echo json_encode(ResponseHttp::status400($errors->all()[0]));
            } else {
                CategoryModel::setNombre($this->getParam()['nombre']);
                CategoryModel::setUbicacion($this->getParam()['ubicacion']);
                echo json_encode(CategoryModel::postSave());
            }

            exit;
        }
    }

    /**************************categoria por ID**************************/
    final public function getCategory(string $endPoint)
    {
        if ($this->getMethod() == 'get' && $endPoint == $this->getRoute()) {
            Security::validateTokenJwt(Security::secretKey());
            $id = $this->getAttribute()[1];
            if (!isset($id)) {
                echo json_encode(ResponseHttp::status400('El id es requerido'));
            } else if (!preg_match(self::$validate_number, $id)) {
                echo json_encode(ResponseHttp::status400('El id solo admite números'));
            } else {
                CategoryModel::setId($id);
                echo json_encode(CategoryModel::getCategory());
                exit;
            }
            exit;
        }
    }
    /************************** Actualizar **************************/
    final public function patchUpdate(string $endPoint)
    {
        if ($this->getMethod() == 'patch' && $this->getRoute() == $endPoint) {

            $validator = new Validator;
            $validator->setMessage('required', ":attribute es requerido.");
            $validator->setMessage('integer', ":attribute debe ser número entero.");
            $validator->setMessage('min', ":attribute , debe ser mínimo :min caracteres.");
            $validator->setMessage('max', ":attribute , debe ser máximo :max caracteres.");

            $validation = $validator->validate($this->getParam(), [
                'id'        => 'required|integer',
                'nombre'        => 'required|min:4|max:50|regex:/^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ ]+$/',
                'ubicacion'      => 'required|min:4|max:50|regex:/^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ ]+$/',
            ]);

            if ($validation->fails()) {
                $errors = $validation->errors();
                echo json_encode(ResponseHttp::status400($errors->all()[0]));
            } else {
                CategoryModel::setId($this->getParam()['id']);
                CategoryModel::setNombre($this->getParam()['nombre']);
                CategoryModel::setUbicacion($this->getParam()['ubicacion']);
                echo json_encode(CategoryModel::patchUpdate());
            }

            exit;
        }
    }

    /**************************Categorias por Texto**************************/
    final public function getCategorySearch(string $endPoint)
    {
        $texto = $_GET['query'] ?? '';
        if ($this->getMethod() == 'get' && $endPoint == $this->getRoute()) {
            Security::validateTokenJwt(Security::secretKey());
            if (!isset($texto) || empty($texto)) {
                echo json_encode(ResponseHttp::status400('query es requerido'));
            } else {
                CategoryModel::setQueryValue($_GET['query']);
                echo json_encode(CategoryModel::getCategorySearch());
                exit;
            }
            exit;
        }
    }

    /************************** Obtener Productos **************************/
    final public function getProducts(string $endPoint)
    {
        $texto = $_GET['category_id']??'';
        if ($this->getMethod() == 'get' && $endPoint == $this->getRoute()) {
            Security::validateTokenJwt(Security::secretKey());
            if (!isset($texto) || empty($texto)) {
                echo json_encode(ResponseHttp::status400('category_id es requerido'));
            } else if (!preg_match(self::$validate_number, $texto)) {
                echo json_encode(ResponseHttp::status400('El category_id solo admite números'));
            } else {
                CategoryModel::setId($texto);
                echo json_encode(CategoryModel::getProducts());
                exit;
            }
            exit;
        }
    }

    /************************** Eliminar **************************/
    final public function deleteCategory(string $endPoint)
    {
        if ($this->getMethod() == 'delete' && $this->getRoute() == $endPoint) {
            Security::validateTokenJwt(Security::secretKey());

            if (empty($this->getParam()['category_id'])) {
                echo json_encode(ResponseHttp::status400('Category_id es requerido'));
            } else {
                CategoryModel::setId($this->getParam()['category_id']);
                echo json_encode(CategoryModel::deleteCategory());
            }
            exit;
        }
    }
}
