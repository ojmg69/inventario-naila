<?php

use App\Config\ResponseHttp;
use App\Controllers\CategoryController;

/*************Parametros enviados por la URL*******************/
$params  = explode('/' ,$_GET['route']);

/*************Instancia del controlador**************/
$app = new CategoryController();

/*************Rutas***************/
$app->getAll('category/');
$app->postSave('category/');
$app->getCategory("category/{$params[1]}");
$app->patchUpdate('category/update/');
$app->getCategorySearch('category/search/');
$app->getProducts('category/products/');
$app->deleteCategory('category/');


/****************Error 404*****************/
echo json_encode(ResponseHttp::status404());