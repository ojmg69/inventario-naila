<?php

namespace App\Models;

use App\Config\ResponseHttp;
use App\Config\Security;
use App\DB\ConnectionDB;
use App\DB\Sql;

class UserModel extends ConnectionDB
{

    //Propiedades de la base de datos
    private static string $nombre;
    private static string $apellido;
    private static string $id;
    private static string $queryValue;
    private static string $correo;
    private static string $usuario;
    private static string $password;
    private static string $IDToken;

    public function __construct(array $data)
    {
        self::$nombre   = $data['nombre'];
        self::$apellido   = $data['apellido'];
        self::$id      = $data['id'];
        self::$queryValue = $data['queryValue'];
        self::$correo   = $data['email'];
        self::$usuario   = $data['usuario'];
        self::$password = $data['password'];
    }

    /**************************Metodos Getter**************************/
    final public static function getNombre()
    {
        return self::$nombre;
    }
    final public static function getApellido()
    {
        return self::$apellido;
    }

    final public static function getId()
    {
        return self::$id;
    }
    final public static function getEmail()
    {
        return self::$correo;
    }
    final public static function getQueryValue()
    {
        return self::$queryValue;
    }
    final public static function getUsuario()
    {
        return self::$usuario;
    }
    final public static function getPassword()
    {
        return self::$password;
    }
    final public static function getIDToken()
    {
        return self::$IDToken;
    }

    /**************************Metodos Setter**************************/
    final public static function setNombre(string $nombre)
    {
        self::$nombre = $nombre;
    }
    final public static function setApellido(string $apellido)
    {
        self::$apellido = $apellido;
    }

    final public static function setId(string $id)
    {
        self::$id = $id;
    }
    final public static function setQueryValue(string $queryValue)
    {
        self::$queryValue = $queryValue;
    }
    final public static function setEmail(string $correo)
    {
        self::$correo = $correo;
    }
    final public static function setUsuario(string $usuario)
    {
        self::$usuario = $usuario;
    }
    final public static function setPassword(string $password)
    {
        self::$password = $password;
    }
    final public static function setIDToken(string $IDToken)
    {
        self::$IDToken = $IDToken;
    }

    /**************************Validar si la contaseña antigua es correcta**************************/
    final public static function validateUserPassword(string $IDToken, string $oldPassword)
    {
        try {
            $con = self::getConnection();
            $query = $con->prepare("SELECT password FROM usuario WHERE IDToken = :IDToken");
            $query->execute([
                ':IDToken' => $IDToken
            ]);

            if ($query->rowCount() == 0) {
                die(json_encode(ResponseHttp::status500()));
            } else {
                $res = $query->fetch(\PDO::FETCH_ASSOC);

                if (Security::validatePassword($oldPassword, $res['password'])) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (\PDOException $e) {
            error_log('UserModel::validateUserPassword -> ' . $e);
            die(json_encode(ResponseHttp::status500()));
        }
    }

    /**************************Login**************************/
    final public static function login()
    {
        try {
            $con = self::getConnection()->prepare("SELECT * FROM usuario WHERE usuario_usuario = :usuario ");
            $con->execute([
                ':usuario' => self::getUsuario()
            ]);

            if ($con->rowCount() === 0) {
                return ResponseHttp::status400('El usuario o contraseña son incorrectos');
            } else {
                foreach ($con as $res) {
                    if (Security::validatePassword(self::getPassword(), $res['usuario_clave'])) {
                        $payload = ['IDToken' => $res['id_token']];
                        $token = Security::createTokenJwt(Security::secretKey(), $payload);

                        $data = [
                            'id'  => $res['usuario_id'],
                            'nombre'  => $res['usuario_nombre'],
                            'apellido'  => $res['usuario_apellido'],
                            'usuario'  => $res['usuario_usuario'],
                            'token' => $token
                        ];
                        return ResponseHttp::status200("completado", $data);
                        exit;
                    } else {
                        return ResponseHttp::status400('El usuario o contraseña son incorrectos');
                    }
                }
            }
        } catch (\PDOException $e) {
            error_log("UserModel::Login -> " . $e);
            die(json_encode(ResponseHttp::status500()));
        }
    }

    /**************************Consultar todos los usuarios**************************/
    final public static function getAll()
    {
        try {
            $con = self::getConnection();
            $query = $con->prepare("SELECT * FROM usuario where usuario_id !=1");
            $query->execute();

            $dat = $query->fetchAll(\PDO::FETCH_ASSOC);
            $data = [];
            foreach ($dat as $res) {

                array_push($data, [
                    'id'  => $res['usuario_id'],
                    'nombre'  => $res['usuario_nombre'],
                    'apellido'  => $res['usuario_apellido'],
                    'usuario'  => $res['usuario_usuario'],
                    'email'  => $res['usuario_email'],

                ]);
            }
            return ResponseHttp::status200('completado',$data);
            exit;
        } catch (\PDOException $e) {
            error_log("UserModel::getAll -> " . $e);
            die(json_encode(ResponseHttp::status500('No se pueden obtener los datos')));
        }
    }

    /**************************usuario por ID**************************/
    final public static function getUser()
    {
        try {
            $con = self::getConnection();
            $query = $con->prepare("SELECT * FROM usuario WHERE usuario_id = :id");
            $query->execute([
                ':id' => self::getId()
            ]);

            if ($query->rowCount() == 0) {
                return ResponseHttp::status400('Usuario No encontrado');
            } else {

                $dat = $query->fetchAll(\PDO::FETCH_ASSOC);
                $data = [];
                foreach ($dat as $res) {

                    array_push($data, [
                        'id'  => $res['usuario_id'],
                        'nombre'  => $res['usuario_nombre'],
                        'apellido'  => $res['usuario_apellido'],
                        'usuario'  => $res['usuario_usuario'],
                        'email'  => $res['usuario_email'],

                    ]);
                }
                return ResponseHttp::status200('completado',$data);
            }
        } catch (\PDOException $e) {
            error_log("UserModel::getUser -> " . $e);
            die(json_encode(ResponseHttp::status500('No se pueden obtener los datos del usuario')));
        }
    }

    /**************************Consultar usuarios por texto**************************/
    final public static function getUserByString()
    {
        try {
            $con = self::getConnection();
            $query = $con->prepare("SELECT * FROM usuario WHERE 
            usuario_nombre LIKE :queryValue
            OR usuario_apellido LIKE :queryValue 
            OR usuario_usuario LIKE :queryValue 
            ORDER BY usuario_nombre ASC");

            $query->execute([
                ':queryValue' =>  '%'.self::getQueryValue().'%'
            ]);
            
           
            if ($query->rowCount() == 0) {
                return ResponseHttp::status400('Usuario No encontrado');
            } else {

                $dat = $query->fetchAll(\PDO::FETCH_ASSOC);
                $data = [];
                foreach ($dat as $res) {

                    array_push($data, [
                        'id'  => $res['usuario_id'],
                        'nombre'  => $res['usuario_nombre'],
                        'apellido'  => $res['usuario_apellido'],
                        'usuario'  => $res['usuario_usuario'],
                        'email'  => $res['usuario_email'],

                    ]);
                }
                return ResponseHttp::status200('completado',$data);
            }
        } catch (\PDOException $e) {
            error_log("UserModel::getUserByString -> " . $e);
            die(json_encode(ResponseHttp::status500('No se pueden obtener los datos del usuario')));
        }
    }

    /**************************Registrar usuario**************************/
    final public static function postSave()
    {
        if (Sql::exists("SELECT usuario_usuario FROM usuario WHERE usuario_usuario = :user", ":user", self::getUsuario())) {
            return ResponseHttp::status400('El Usuario ya esta registrado');
        } else if (Sql::exists("SELECT usuario_email FROM usuario WHERE usuario_email = :correo", ":correo", self::getEmail())) {
            return ResponseHttp::status400('El Correo ya esta registrado');
        } else {
            self::setIDToken(hash('sha512', self::getUsuario() . self::getEmail()));

            try {
                $con = self::getConnection();
                $query1 = "INSERT INTO usuario (usuario_nombre,usuario_apellido,usuario_usuario,usuario_clave,usuario_email) VALUES";
                $query2 = "(:nombre,:apellido,:usuario,:clave,:email)";
                $query = $con->prepare($query1 . $query2);
                $query->execute([
                    ':nombre'  => self::getNombre(),
                    ':apellido'     => self::getApellido(),
                    ':usuario'     => self::getUsuario(),
                    ':email'  => self::getEmail(),
                    ':clave' => Security::createPassword(self::getPassword()),

                ]);
                if ($query->rowCount() > 0) {
                    return ResponseHttp::status200('Usuario registrado exitosamente');
                } else {
                    return ResponseHttp::status500('No se puede registrar el usuario');
                }
            } catch (\PDOException $e) {
                error_log('UserModel::post -> ' . $e);
                die(json_encode(ResponseHttp::status500()));
            }
        }
    }

    /******************************Actualizar Usuario********************************/
    final public static function patchUser()
    {
        try {
            $con = self::getConnection();

            //buscar usuario y mantener contraseña antigua
            $query1 = $con->prepare("SELECT * FROM usuario WHERE usuario_id = :id");
            $query1->execute([
                ':id' => self::getId()
            ]);
            $dat = $query1->fetchAll(\PDO::FETCH_ASSOC);
            $oldPass=$dat[0]['usuario_clave'];
            if(!empty(self::getPassword())){
                $newPass=Security::createPassword(self::getPassword());

            }else{
                $newPass=$oldPass;
            }
   
            $query = $con->prepare("UPDATE usuario SET usuario_nombre=:nombre,usuario_apellido=:apellido,usuario_usuario=:usuario,
            usuario_clave=:clave,usuario_email=:email WHERE usuario_id = :id");
            $query->execute([
                ':id'=>self::getId(),
                ':clave' => $newPass,
                ':nombre'  => self::getNombre(),
                ':apellido'     => self::getApellido(),
                ':usuario'     => self::getUsuario(),
                ':email'  => self::getEmail(),

                //':IDToken'  => self::getIDToken()
            ]);
            if ($query->rowCount() > 0) {
                return ResponseHttp::status200('Usuario actualizado exitosamente');
            } else {
                return ResponseHttp::status500('Error al actualizar la contraseña del usuario');
            }
        } catch (\PDOException $e) {
            error_log("UserModel::patchUser -> " . $e);
            die(json_encode(ResponseHttp::status500()));
        }
    }

    /**************************Actualizar la contraseña de usuario**************************/
    final public static function patchPassword()
    {
        try {
            $con = self::getConnection();
            $query = $con->prepare("UPDATE usuario SET password = :password WHERE IDToken = :IDToken");
            $query->execute([
                ':password' => Security::createPassword(self::getPassword()),
                ':IDToken'  => self::getIDToken()
            ]);
            if ($query->rowCount() > 0) {
                return ResponseHttp::status200('Contraseña actualizado exitosamente');
            } else {
                return ResponseHttp::status500('Error al actualizar la contraseña del usuario');
            }
        } catch (\PDOException $e) {
            error_log("UserModel::patchPassword -> " . $e);
            die(json_encode(ResponseHttp::status500()));
        }
    }

    /************************** Eliminar**************************/
    final public static function deleteUser()
    {
        try {
            $con   = self::getConnection();
            $query = $con->prepare("DELETE FROM usuario WHERE usuario_id = :id");
            $query->execute([
                ':id' => self::getId()
            ]);

            if ($query->rowCount() > 0) {
                return ResponseHttp::status200('Usuario eliminado exitosamente');
            } else {
                return ResponseHttp::status500('Usuario no encontrado');
            }
        } catch (\PDOException $e) {
            error_log("UserModel::deleteUser -> " . $e);
            die(json_encode(ResponseHttp::status500('No se puede eliminar el usuario')));
        }
    }
}
