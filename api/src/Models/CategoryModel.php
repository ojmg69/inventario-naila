<?php

namespace App\Models;

use App\Config\ResponseHttp;
use App\Config\Security;
use App\DB\ConnectionDB;
use App\DB\Sql;

class CategoryModel extends ConnectionDB
{

    //Propiedades de la base de datos
    private static string $nombre;
    private static string $ubicacion;
    private static int $id;
    private static string $queryValue;


    public function __construct(array $data)
    {
        self::$nombre   = $data['nombre'];
        self::$ubicacion      = $data['ubicacion'];
        self::$id      = $data['id'];
        self::$queryValue = $data['queryValue'];
    }

    /**************************Metodos Getter**************************/
    final public static function getNombre()
    {
        return self::$nombre;
    }
    final public static function getId()
    {
        return self::$id;
    }
    final public static function getUbicacion()
    {
        return self::$ubicacion;
    }
    final public static function getQueryValue()
    {
        return self::$queryValue;
    }



    /**************************Metodos Setter**************************/
    final public static function setNombre(string $nombre)
    {
        self::$nombre = $nombre;
    }
    final public static function setUbicacion(string $ubicacion)
    {
        self::$ubicacion = $ubicacion;
    }

    final public static function setId(string $id)
    {
        self::$id = $id;
    }
    final public static function setQueryValue(string $queryValue)
    {
        self::$queryValue = $queryValue;
    }


    /************************** Listado **************************/
    final public static function getAll()
    {
        try {
            $con = self::getConnection();
            $query = $con->prepare("SELECT * FROM categoria ORDER BY categoria_nombre ASC");
            $query->execute();

            $dat = $query->fetchAll(\PDO::FETCH_ASSOC);
            $data = [];
            foreach ($dat as $res) {

                array_push($data, [
                    'id'  => $res['categoria_id'],
                    'nombre'  => $res['categoria_nombre'],
                    'ubicacion'  => $res['categoria_ubicacion'],

                ]);
            }
            return ResponseHttp::status200('completado',$data);
            exit;
        } catch (\PDOException $e) {
            error_log("CategoryModel::getAll -> " . $e);
            die(json_encode(ResponseHttp::status500('No se pueden obtener los datos')));
        }
    }


    /************************** Nuevo **************************/
    final public static function postSave()
    {
        if (Sql::exists("SELECT categoria_nombre FROM categoria WHERE categoria_nombre = :user", ":user", self::getNombre())) {
            return ResponseHttp::status400('El NOMBRE ingresado ya se encuentra registrado, por favor elija otro');
        } else {

            try {
                $con = self::getConnection();
                $query1 = "INSERT INTO categoria (categoria_nombre,categoria_ubicacion) VALUES";
                $query2 = "(:nombre,:ubicacion)";
                $query = $con->prepare($query1 . $query2);
                $query->execute([
                    ':nombre'  => self::getNombre(),
                    ':ubicacion'     => self::getUbicacion(),

                ]);
                if ($query->rowCount() > 0) {
                    $lasId=$con->lastInsertId();
                    $query3 = $con->prepare("SELECT * FROM categoria WHERE categoria_id = :id");
                    $query3->execute([
                        ':id' => $lasId
                    ]);
                    $dat = $query3->fetchAll(\PDO::FETCH_ASSOC);
                    $data = [];
                    foreach ($dat as $res) {
    
                        array_push($data, [
                            'id'  => $res['categoria_id'],
                            'nombre'  => $res['categoria_nombre'],
                            'ubicacion'  => $res['categoria_ubicacion'],
    
                        ]);
                    }
                    return ResponseHttp::status200('Categoría registrada exitosamente',$data);
                } else {
                    return ResponseHttp::status500('No se puede registrar la categoría');
                }
            } catch (\PDOException $e) {
                error_log('CategoryModel::post -> ' . $e);
                die(json_encode(ResponseHttp::status500()));
            }
        }
    }

    /**************************Categoria por ID**************************/
    final public static function getCategory()
    {
        try {
            $con = self::getConnection();
            $query = $con->prepare("SELECT * FROM categoria WHERE categoria_id = :id");
            $query->execute([
                ':id' => self::getId()
            ]);

            if ($query->rowCount() == 0) {
                return ResponseHttp::status400('Categoría No encontrada');
            } else {

                $dat = $query->fetchAll(\PDO::FETCH_ASSOC);
                $data = [];
                foreach ($dat as $res) {

                    array_push($data, [
                        'id'  => $res['categoria_id'],
                        'nombre'  => $res['categoria_nombre'],
                        'ubicacion'  => $res['categoria_ubicacion'],

                    ]);
                }
                return ResponseHttp::status200('completado',$data);
            }
        } catch (\PDOException $e) {
            error_log("CategoryModel::getCategory -> " . $e);
            die(json_encode(ResponseHttp::status500('No se pueden obtener los datos de la categoria')));
        }
    }


    /****************************** Actualizar **************************/
    final public static function patchUpdate()
    {
        try {
            $con1 = self::getConnection();
            $query1 = $con1->prepare("SELECT * FROM categoria WHERE categoria_id = :id");
            $query1->execute([
                ':id' => self::getId()
            ]);


            if ($query1->rowCount() == 0) {
                return ResponseHttp::status400('Categoría No encontrada');
            } else {
                $con = self::getConnection();
            $query = $con->prepare("UPDATE categoria SET categoria_nombre=:nombre,
            categoria_ubicacion=:ubicacion
             WHERE categoria_id = :id");
                $query->execute([
                    ':id' => self::getId(),
                    ':nombre'  => self::getNombre(),
                    ':ubicacion'     => self::getUbicacion(),
                ]);
                if ($query->rowCount() > 0) {
                    $query3 = $con->prepare("SELECT * FROM categoria WHERE categoria_id = :id");
                    $query3->execute([
                        ':id' => self::getId(),
                    ]);
                    $dat = $query3->fetchAll(\PDO::FETCH_ASSOC);
                    $data = [];
                    foreach ($dat as $res) {
    
                        array_push($data, [
                            'id'  => $res['categoria_id'],
                            'nombre'  => $res['categoria_nombre'],
                            'ubicacion'  => $res['categoria_ubicacion'],
    
                        ]);
                    }
                    return ResponseHttp::status200('Categoría actualizada exitosamente',$data);
                } else {
                    return ResponseHttp::status500('No se ha podido actualizar categoría');
                }
            }
        } catch (\PDOException $e) {
            error_log("CategoryModel::patchUpdate -> " . $e);
            die(json_encode(ResponseHttp::status500()));
        }
    }

     /************************** categoria por texto **************************/
     final public static function getCategorySearch()
     {
         try {
             $con = self::getConnection();
             $query = $con->prepare("SELECT * FROM categoria WHERE 
             categoria_nombre LIKE :queryValue
             OR categoria_ubicacion LIKE :queryValue              
             ORDER BY categoria_nombre ASC");
 
             $query->execute([
                 ':queryValue' =>  '%'.self::getQueryValue().'%'
             ]);
             
            
             if ($query->rowCount() == 0) {
               return ResponseHttp::status400('Categoría No encontrada');
             } else {
 
                 $dat = $query->fetchAll(\PDO::FETCH_ASSOC);
                 $data = [];
                 foreach ($dat as $res) {
 
                     array_push($data, [
                        'id'  => $res['categoria_id'],
                        'nombre'  => $res['categoria_nombre'],
                        'ubicacion'  => $res['categoria_ubicacion']
                     ]);
                 }
                 return ResponseHttp::status200('completado',$data);
             }
         } catch (\PDOException $e) {
             error_log("CategoryModel::getCategorySearch -> " . $e);
             die(json_encode(ResponseHttp::status500('No se pueden obtener los datos de la categoria')));
         }
     }

     /************************** Obtener  Productos **************************/
     final public static function getProducts()
     {
         try {
            $con1 = self::getConnection();
            $query1 = $con1->prepare("SELECT * FROM categoria WHERE categoria_id = :id");
            $query1->execute([
                ':id' => self::getId()
            ]);


            if ($query1->rowCount() == 0) {
                return ResponseHttp::status400('Categoría No encontrada');
            } else {
                
             $con = self::getConnection();
             $query = $con1->prepare("SELECT * FROM producto WHERE categoria_id = :id");
             $query->execute([
                 ':id' => self::getId()
             ]);
             
            
             if ($query->rowCount() == 0) {
               return ResponseHttp::status400('Productos No encontrada');
             } else {
 
                 $dat = $query->fetchAll(\PDO::FETCH_ASSOC);
                 $data = [];
                 foreach ($dat as $res) {
 
                     array_push($data, [
                        'id'  => $res['producto_id'],
                        'categoria_id'  => $res['categoria_id'],
                        'nombre'  => $res['producto_nombre'],
                        'codigo'  => $res['producto_codigo']
                     ]);
                 }
                 return ResponseHttp::status200('completado',$data);
             }
             }
         } catch (\PDOException $e) {
             error_log("CategoryModel::getCategorySearch -> " . $e);
             die(json_encode(ResponseHttp::status500('No se pueden obtener los datos de la categoria')));
         }
     }

         /************************** Eliminar**************************/
    final public static function deleteCategory()
    {
        try {
            $con   = self::getConnection();
            $query = $con->prepare("DELETE FROM categoria WHERE categoria_id = :id");
            $query->execute([
                ':id' => self::getId()
            ]);

            if ($query->rowCount() > 0) {
                return ResponseHttp::status200('Categoría eliminada exitosamente');
            } else {
                return ResponseHttp::status500('Categoría no encontrada');
            }
        } catch (\PDOException $e) {
            error_log("CategoryModel::deleteCategory -> " . $e);
            die(json_encode(ResponseHttp::status500('No se puede eliminar la Categoría')));
        }
    }
}
