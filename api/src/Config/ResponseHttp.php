<?php

namespace App\Config;

class ResponseHttp {

    public static $message = array(
        'status' => '',
        'message' => '',
        'data' => ''
    );


    public static function status200($message,array $data=[])
    {
        http_response_code(200);
        self::$message['status'] = 'ok';
        self::$message['message'] = $message;
        self::$message['data'] =$data;
        return self::$message;
    }

    public static function status201(string $message = 'Recurso creado',array $data=[])
    {
        http_response_code(201);
        self::$message['status'] = 'ok';
        self::$message['message'] = $message;
        self::$message['data'] =$data;
        return self::$message;
    }

    public static function status400(string $message = 'solicitud enviada incompleta o en formato incorrecto',array $data=[])
    {
        http_response_code(400);
        self::$message['status'] = 'error';
        self::$message['message'] = $message;
        self::$message['data'] =$data;
        return self::$message;
    }

    public static function status401(string $message = 'No tiene privilegios para acceder al recurso solicitado',array $data=[])
    {
        http_response_code(401);
        self::$message['status'] = 'error';
        self::$message['message'] = $message;
        self::$message['data'] =$data;
        return self::$message;
    }

    public static function status404(string $message = 'Bad request',array $data=[])
    {
        http_response_code(404);
        self::$message['status'] = 'error';
        self::$message['message'] = $message;
        self::$message['data'] =$data;
        return self::$message;
    }

    public static function status500(string $message = 'Error interno del servidor',array $data=[])
    {
        http_response_code(500);
        self::$message['status'] = 'error';
        self::$message['message'] = $message;
        self::$message['data'] =$data;
        return self::$message;
    }   
}