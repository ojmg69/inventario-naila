<?php
	/*== Almacenando datos ==*/
    $sub_category_id_del=limpiar_cadena($_GET['sub_category_id_del']);

    /*== Verificando usuario ==*/
    $check_sub_categoria=conexion();
    $check_sub_categoria=$check_sub_categoria->query("SELECT sub_categoria_id FROM sub_categoria WHERE sub_categoria_id='$sub_category_id_del'");
    
    if($check_sub_categoria->rowCount()==1){

    	$check_productos=conexion();
    	$check_productos=$check_productos->query("SELECT sub_categoria_id FROM producto WHERE sub_categoria_id='$sub_category_id_del' LIMIT 1");

    	if($check_productos->rowCount()<=0){

    		$eliminar_sub_categoria=conexion();
	    	$eliminar_sub_categoria=$eliminar_sub_categoria->prepare("DELETE FROM sub_categoria WHERE sub_categoria_id=:id");

	    	$eliminar_sub_categoria->execute([":id"=>$sub_category_id_del]);

	    	if($eliminar_sub_categoria->rowCount()==1){
		        echo '
		            <div class="notification is-info is-light">
		                <strong>¡CATEGORIA ELIMINADA!</strong><br>
		                Los datos de la categoría se eliminaron con exito
		            </div>
		        ';
		    }else{
		        echo '
		            <div class="notification is-danger is-light">
		                <strong>¡Ocurrio un error inesperado!</strong><br>
		                No se pudo eliminar la categoría, por favor intente nuevamente
		            </div>
		        ';
		    }
		    $eliminar_sub_categoria=null;
    	}else{
    		echo '
	            <div class="notification is-danger is-light">
	                <strong>¡Ocurrio un error inesperado!</strong><br>
	                No podemos eliminar la categoría ya que tiene productos asociados
	            </div>
	        ';
    	}
    	$check_productos=null;
    }else{
    	echo '
            <div class="notification is-danger is-light">
                <strong>¡Ocurrio un error inesperado!</strong><br>
                La CATEGORIA que intenta eliminar no existe
            </div>
        ';
    }
    $check_sub_categoria=null;