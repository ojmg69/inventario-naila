<div class="container is-fluid mb-6">
    <h1 class="title">Productos</h1>
    <h2 class="subtitle">Lista de productos por Sub categoría</h2>
</div>

<div class="container pb-6 pt-6">
    <?php
        require_once "./php/main.php";
    ?>
    <div class="columns">
        <div class="column is-one-third">
            <h2 class="title has-text-centered">Sub Categorías</h2>
            <?php
                $sub_categoria=conexion();
                $sub_categoria=$sub_categoria->query("SELECT * FROM sub_categoria");
                if($sub_categoria->rowCount()>0){
                    $sub_categoria=$sub_categoria->fetchAll();
                    foreach($sub_categoria as $row){
                        echo '<a href="index.php?vista=product_sub_category&sub_category_id='.$row['sub_categoria_id'].'" class="button is-link is-inverted is-fullwidth">'.$row['sub_categoria_nombre'].'</a>';
                    }
                }else{
                    echo '<p class="has-text-centered" >No hay sub categorías registradas</p>';
                }
                $sub_categoria=null;
            ?>
        </div>
        <div class="column">
            <?php
                $sub_categoria_id = (isset($_GET['sub_category_id'])) ? $_GET['sub_category_id'] : 0;

                /*== Verificando categoria ==*/
                $check_sub_categoria=conexion();
                $check_sub_categoria=$check_sub_categoria->query("SELECT * FROM sub_categoria WHERE sub_categoria_id='$sub_categoria_id'");

                if($check_sub_categoria->rowCount()>0){

                    $check_sub_categoria=$check_sub_categoria->fetch();

                    echo '
                        <h2 class="title has-text-centered">'.$check_sub_categoria['sub_categoria_nombre'].'</h2>
                        <p class="has-text-centered pb-6" >'.$check_sub_categoria['sub_categoria_ubicacion'].'</p>
                    ';

                    require_once "./php/main.php";

                    # Eliminar producto #
                    if(isset($_GET['product_id_del'])){
                        require_once "./php/producto_eliminar.php";
                    }

                    if(!isset($_GET['page'])){
                        $pagina=1;
                    }else{
                        $pagina=(int) $_GET['page'];
                        if($pagina<=1){
                            $pagina=1;
                        }
                    }

                    $pagina=limpiar_cadena($pagina);
                    $url="index.php?vista=product_sub_category&sub_category_id=$sub_categoria_id&page="; /* <== */
                    $registros=15;
                    $busqueda="";

                    # Paginador producto #
                    require_once "./php/producto_lista.php";

                }else{
                    echo '<h2 class="has-text-centered title" >Seleccione una categoría para empezar</h2>';
                }
                $check_sub_categoria=null;
            ?>
        </div>
    </div>
</div>