<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" href="css/Login.css">
	<div class="main-container">

	<form action="" method="POST">
  </head>
  <body>

    <div class="Login">
        <img src="img/Logo_SCIV.jpg" class="php" alt="php Image">
        <h1>Iniciar Sesion</h1>
        <form action="index.php">
            <label for="username">Usuario</label>
            <input type="text" placeholder="Ingresa tu Usuario" name="login_usuario" pattern="[a-zA-Z0-9]{4,20}" maxlength="20" required >
            <label for="password">Contraseña</label>
            <input type="password" placeholder="Ingresa tu contraseña" name="login_clave" pattern="[a-zA-Z0-9$@.-]{7,100}" maxlength="100" required >

            
            <input type="submit" value="Ingresar">
            <a href="recuperar.php">¿Olvidaste la contraseña?</a><br>
            <a href="registrarse.php">Registrarse</a>
        </form>
    </div>
  </body> 
  <?php
			if(isset($_POST['login_usuario']) && isset($_POST['login_clave'])){
				require_once "./php/main.php";
				require_once "./php/iniciar_sesion.php";
			}
		?>
</html>